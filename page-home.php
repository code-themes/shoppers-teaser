<?php
/*
 Template Name: Home page
*/
?>

<?php get_header(); ?>

  <div class="md:px-12 pt-12 pb-20 bg-primary-500">
    <div class="-mt-12 px-6 md:px-20 py-5 text-right">
      <?php wp_nav_menu(array(
          'container' => false, // remove nav container
          'menu' => __('Main Menu', 'platetheme'), // nav name
          'menu_class' => 'top-bar', // adding custom nav class
          'theme_location' => 'main-nav', // where it's located in the theme
          'depth' => 0, // limit the depth of the nav
      )); ?>
    </div>
    <div class="bg-white md:rounded-xl md:px-20 pt-40 pb-56">
      <div class="container max-w-4xl">

        <?php $text = get_field('teaser_text');
        if (!empty($text)): ?>
          <div
            class="uppercase md:text-lg text-center font-medium tracking-extra-wide leading-none"
          >
            <?php echo $text; ?>
          </div>
        <?php endif; ?>

        <div class="mt-5 md:mt-2">
          <?php echo load_svg('src/images/logo-slogan.svg', 'h-24 sm:h-32 md:h-40 w-auto max-w-full mx-auto'); ?>
        </div>

        <div class="mt-20">
            
          <main id="main" class="main" role="main" itemscope itemprop="mainContentOfPage" itemtype="https://schema.org/Blog">

            <?php // Edit the loop in /templates/block-loop. Or roll your own. ?>
            <?php get_template_part( 'templates/full-loop'); ?>

          </main>

        </div>

        <div class="mt-20">
          <?php get_template_part('templates/opt-in'); ?>
        </div>
      </div>
    </div>
  </div>

  <div class="-mt-56">
    <?php get_template_part('templates/slider'); ?>
  </div>

  <?php $heading = get_field('career_heading');
  if (!empty($heading)): ?>
    <div class="md:px-12 mx-auto my-40 max-w-6xl">
      <div class="bg-platinum md:rounded-xl shadow-xl overflow-hidden">
        <?php $image = get_field('career_image');
        if (!empty($image)): ?>
          <?php echo wp_get_attachment_image($image['id'], 'plate-image-1056', false, array('class' => 'w-full')); ?>
        <?php endif; ?>

        <div
          class="px-6 sm:px-16 py-9 bg-primary-500 text-white text-center font-semibold leading-tight"
        >
          <h2 class="text-3xl">
            <?php echo $heading; ?>
          </h2>
        </div>

        <div class="py-16">
          <?php if (have_rows('career_benefits')): ?>
            <div class="mx-auto px-6 sm:px-16 max-w-3xl space-y-16">
              <?php while (have_rows('career_benefits')): the_row(); 
              $icon = get_sub_field('icon');
              $content = get_sub_field('content');
              ?>
                <div class="flex items-center">
                  <?php if (!empty($icon)): ?>
                    <div class="flex-shrink-0 mr-7 sm:mr-15">
                      <?php echo load_svg('src/images/career/'.$icon.'.svg', 'w-16 h-auto'); ?>
                    </div>
                  <?php endif; ?>
                  <div>
                    <?php if (!empty($content['heading'])): ?>
                      <h3 class="font-medium text-primary-500 text-2xl leading-tight">
                        <?php echo $content['heading']; ?>
                      </h3>
                    <?php endif; ?>

                    <?php if (!empty($content['text'])): ?>
                      <p class="mt-1 font-light text-lg">
                        <?php echo $content['text']; ?>
                      </p>
                    <?php endif; ?>
                  </div>
                </div>
              <?php endwhile; ?>
            </div>
          <?php endif; ?>

          <?php $button = get_field('career_button');
          if ($button): ?>
            <div class="mt-20 px-6 sm:px-16 text-center">
              <a
                href="<?php echo esc_url($button['url']); ?>"
                target="<?php echo esc_attr($button['target'] ? $button['target'] : '_self'); ?>"
                class="block sm:inline-block px-13 py-6 text-2xl leading-6 text-white font-semibold rounded-lg bg-primary-500 hover:bg-primary-600 focus:outline-none focus:shadow-outline-primary focus:bg-primary-600 transition duration-150 ease-in-out"
              >
                <?php echo esc_html($button['title']); ?>
              </a>
            </div>
          <?php endif; ?>

          <?php if (have_rows('career_positions')): ?>
            <div
              class="mt-20 flex flex-col sm:flex-row justify-between items-end text-center space-y-12 sm:space-y-0 sm:space-x-12 px-6 sm:px-16"
            >
              <?php while (have_rows('career_positions')): the_row(); 
              $icon = get_sub_field('icon');
              $content = get_sub_field('content');
              ?>
                <div>
                  <?php if (!empty($icon)): ?>
                    <div class="mb-7">
                      <?php echo load_svg('src/images/career/'.$icon.'.svg', 'h-16 w-auto mx-auto'); ?>
                    </div>
                  <?php endif; ?>

                  <div>
                    <?php if (!empty($content['heading'])): ?>
                      <h3 class="text-2xl leading-tight font-medium text-primary-500">
                        <?php echo $content['heading']; ?>
                      </h3>
                    <?php endif; ?>

                    <?php if (!empty($content['text'])): ?>
                      <p class="mt-1 text-lg font-light leading-relaxed">
                        <?php echo $content['text']; ?>
                      </p>
                    <?php endif; ?>
                  </div>
                </div>
              <?php endwhile; ?>
            </div>
          <?php endif; ?>
        </div>
      </div>
    </div>
  <?php endif; ?>

  <?php if (have_rows('contact_items')): ?>
    <div class="container my-40 max-w-4xl">
      <div class="contact first-child-mt-0 last-child-mb-0">
        <?php while (have_rows('contact_items')): the_row(); 
        $text = get_sub_field('text');
        $email = get_sub_field('email');
        ?>
          <?php echo $text; ?>

          <?php if (!empty($email['email_address'])): ?>
            <div class="-mt-1 text-2xl sm:text-3xl">
              <a href="mailto:<?php echo $email['email_address']; ?><?php echo !empty($email['subject']) ? '?subject='.rawurlencode($email['subject']) : ''; ?>"><?php echo $email['email_address']; ?></a
              >
            </div>
          <?php endif; ?>
        <?php endwhile; ?>
      </div>
    </div>
  <?php endif; ?>

<?php get_footer(); ?>