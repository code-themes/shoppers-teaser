��    S      �  q   L                *  !   1  4   S     �     �     �     �     �     �  +   	     5  	   C     M     ^     p     �     �     �  
   �     �     �     �     �  	   
	     	     #	     =	     P	     e	  	   w	     �	     �	     �	  	   �	     �	  	   �	  	   �	     �	     �	     

     
     )
     5
     C
     `
     ~
     �
  	   �
     �
     �
     �
     �
     �
     �
               =     P  	   a     k     �     �  
   �     �     �     �     �       <     '   M     u     �     �     �     �  
   �  $   �  7   �     5     ;     M  �  b     �       !     4   8     m     �     �     �     �     �  +   �       	   (     2     I     [     w     �     �  
   �     �     �     �     �  	   �     �          (     ;     P  
   b     m     �     �  	   �     �  	   �  	   �     �     �     �               %     3     9     ?     R     ^     j     �     �     �     �     �     �     �     �       	   "     ,     F     L  
   [     f     |  	   �     �     �  <   �  -   �          $     1     H     ^  
   l  $   w  7   �     �     �     �     D       &      C   Q          :       
               	   5                         @   9   4      J   8   )                 *   -      B         K       G               E       (         L   1   <             6   3   ;      H   I                      %      N                      M      /   "   #   >            O   !       +   0   R   '   $   .   2       =   F               ?       A             P   S   ,   7        &larr; Previous Comments (Edit) <cite class="fn">%1$s</cite> %2$s <span class="screen-reader-text">Posted on</span> %s <span>%</span> Comments <span>No</span> Comments <span>One</span> Comment Add New Grouping Add New Staff Add or remove groupings Add some widgets and they will appear here. All Groupings All Staff Back to overview Background Styles Choose Homepage Preference: Choose from the most used Comments are closed. Edit Grouping Edit Staff F jS, Y Featured Image Filed under: Filter staff list Groupings Groupings list Groupings list navigation Header Text Styles Homepage Preferences Insert into staff Main Menu More Comments &rarr; Move along, move along! New Grouping Name New Staff No items Not Found Not found Not found in Trash Parent Grouping Parent Grouping: Parent Staff: Plate Staff Popular Items Post Type General NameStaff Post Type Singular NameStaff Posts Categorized: Primary Menu  Read more Remove featured image Reply Search Groupings Search Results for: Search Staff Select Blog Homepage: Select Homepage: Separate groupings with commas Set featured image Site Description Site Name Site Name and Description Staff Staff Archives Staff list Staff list navigation Tags: Taxonomy General NameGroupings Taxonomy Singular NameGrouping Theme Settings This is the error message in the templates/404.php template. This isn't the page you're looking for. Update Grouping Update Staff Uploaded to this staff Use as featured image View Grouping View Staff Your comment is awaiting moderation. Your query didn't return anything. Try checking things. by %s labelSearch for: submit buttonSearch Project-Id-Version: Plate
PO-Revision-Date: 2020-05-02 19:53+0200
Last-Translator: 
Language-Team: 
Language: en_US
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.3
X-Poedit-Basepath: ../..
X-Poedit-Flags-xgettext: --add-comments=translators:
X-Poedit-WPHeader: style.css
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.js
 &larr; Previous Comments (Edit) <cite class="fn">%1$s</cite> %2$s <span class="screen-reader-text">Posted on</span> %s <span>%</span> Comments <span>No</span> Comments <span>One</span> Comment Add New Grouping Add New Staff Add or remove groupings Add some widgets and they will appear here. All Groupings All Staff Zurück zur Übersicht Background Styles Choose Homepage Preference: Choose from the most used Comments are closed. Edit Grouping Edit Staff F jS, Y Featured Image Filed under: Filter staff list Groupings Groupings list Groupings list navigation Header Text Styles Homepage Preferences Insert into staff Hauptmenü More Comments &rarr; Weiterfahren, weiterfahren! New Grouping Name New Staff No items Not Found Not found Not found in Trash Parent Grouping Parent Grouping: Parent Staff: Plate Staff Popular Items Staff Staff Posts Categorized: Hauptmenü  Weiterlesen Remove featured image Reply Search Groupings Search Results for: Search Staff Select Blog Homepage: Select Homepage: Separate groupings with commas Set featured image Site Description Site Name Site Name and Description Staff Staff Archives Staff list Staff list navigation Tags: Groupings Grouping Theme Einstellungen This is the error message in the templates/404.php template. Dies ist nicht die Seite, nach der Ihr sucht. Update Grouping Update Staff Uploaded to this staff Use as featured image View Grouping View Staff Your comment is awaiting moderation. Your query didn't return anything. Try checking things. by %s Search for: Search 