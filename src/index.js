import "./styles.css";

// Tiny Slider
// See https://github.com/ganlanyuan/tiny-slider
import "../node_modules/tiny-slider/dist/tiny-slider.css";
import { tns } from "../node_modules/tiny-slider/src/tiny-slider.js";

if (document.querySelector(".slider") !== null) {
  var slider = tns({
    loop: false,
    items: 1,
    gutter: 24,
    edgePadding: 48,
    slideBy: "page",
    mouseDrag: true,
    swipeAngle: false,
    responsive: {
      640: {
        items: 2,
        gutter: 28,
        edgePadding: 64,
      },
      1024: {
        items: 3,
        gutter: 32,
        edgePadding: 96,
      },
      1920: {
        items: 4,
        gutter: 36,
        edgePadding: 112,
      },
      2560: {
        items: 5,
        gutter: 40,
        edgePadding: 128,
      },
    },
  });
}
