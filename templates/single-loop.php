<?php
/**
 * 
 * Template Part: Single
 * Description: Loop code for Single Posts.
 * 
 * @example <?php get_template_part( 'templates/single', 'loop'); ?>
 * 
 * @author  Joshua Michaels for studio.bio <info@studio.bio>
 * @since   1.0.0
 * @version 1.3
 * @license WTFPL
 * 
 * @see     https://konstantin.blog/2013/get_template_part/
 *          http://buildwpyourself.com/get-template-part/
 * 
 */
?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

  <div class="container my-20 max-w-4xl">
    <div class="-mt-6 mb-6">
      <a
        href="<?php echo home_url(); ?>"
        class="group inline-flex items-center -ml-1 text-sm font-medium hover:text-primary-500 focus:outline-none focus:text-primary-600 transition duration-150 ease-in-out"
      >
        <svg
          viewBox="0 0 20 20"
          fill="currentColor"
          class="h-6 w-6 mr-1 text-dark group-hover:text-primary-500 transition duration-150 ease-in-out"
        >
          <path
            fill-rule="evenodd"
            d="M12.707 5.293a1 1 0 010 1.414L9.414 10l3.293 3.293a1 1 0 01-1.414 1.414l-4-4a1 1 0 010-1.414l4-4a1 1 0 011.414 0z"
            clip-rule="evenodd"
          />
        </svg>
        <?php echo __('Back to overview', 'platetheme'); ?>
      </a>
    </div>

  	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?> role="article" itemscope itemprop="blogPost" itemtype="https://schema.org/BlogPosting">

  		<header class="article-header entry-header post first-child-mt-0">

  			<h1 class="entry-title single-title" itemprop="headline" rel="bookmark"><?php the_title(); ?></h1>
      
  		</header> <?php // end article header ?>

      <section class="entry-content post last-child-mb-0" itemprop="articleBody">

      	<?php if ( has_post_format()) { 
      		get_template_part( 'format', get_post_format() ); 
      	} ?>

      	<?php the_content(); ?>

      </section> <?php // end article section ?>

      <footer class="article-footer">

      </footer> <?php // end article footer ?>

    </article> <?php // end article ?>

   </div>

<?php endwhile; ?>

<?php else : ?>

    <?php get_template_part( 'templates/404'); ?>

<?php endif; ?>