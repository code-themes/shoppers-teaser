<?php
$the_query = new WP_Query(array(
  'post_type' => 'post',
  'post_status' => 'publish',
  'posts_per_page'=> -1,
  'order'=>'DESC',
  'orderby'=>'ID',
));
?>
<?php if ($the_query->have_posts()): ?>
  <div class="slider">
    <?php while ($the_query->have_posts()): $the_query->the_post(); ?>
      <div>
        <div
          class="flex flex-col bg-white rounded-xl overflow-hidden shadow-xl h-full"
        >
          <a
            href="<?php the_permalink(); ?>"
            class="block flex-shrink-0 h-56 rounded-t-xl overflow-hidden group focus:outline-none fix-safari-border"
          >
            <?php the_post_thumbnail('plate-image-400', array('class' => 'h-56 w-full object-cover transform group-hover:scale-105 group-focus:scale-105 transition duration-300 ease-in-out')); ?>
          </a>
          <div class="px-7 py-6 flex-grow flex flex-col justify-between">
            <div>
              <h3 class="text-lg leading-relaxed font-bold text-primary-500">
                <?php the_title(); ?>
              </h3>
              <p class="mt-4 text-sm leading-relaxed font-light">
                <?php echo get_the_excerpt(); ?>
              </p>
            </div>
            <div class="mt-4 text-lg leading-relaxed">
              <a
                href="<?php the_permalink(); ?>"
                class="group relative flex items-center font-bold text-primary-500 hover:text-primary-600 focus:outline-none focus:text-primary-600 transition duration-150 ease-in-out"
              >
                <span
                  class="absolute inset-y-0 inset-left flex items-center transform group-hover:translate-x-2.5 group-focus:translate-x-2.5 transition duration-150 ease-in-out"
                >
                  <svg
                    viewBox="0 0 20 20"
                    fill="currentColor"
                    class="h-auto w-6 text-primary-500 group-hover:text-primary-600 group-focus:text-primary-600 transition duration-150 ease-in-out"
                  >
                    <path
                      fill-rule="evenodd"
                      d="M10.293 3.293a1 1 0 011.414 0l6 6a1 1 0 010 1.414l-6 6a1 1 0 01-1.414-1.414L14.586 11H3a1 1 0 110-2h11.586l-4.293-4.293a1 1 0 010-1.414z"
                      clip-rule="evenodd"
                    />
                  </svg>
                </span>
                <span class="pl-11">
                  <?php echo __('Read more', 'platetheme'); ?>
                </span>
              </a>
            </div>
          </div>
        </div>
      </div>
    <?php endwhile; ?>
  </div>
<?php endif; ?>
<?php
// Restore original Post Data
wp_reset_postdata();
?>
