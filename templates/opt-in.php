<?php
$form_action_url = get_field('mailchimp_form_action_url', 'option');;
$parts = parse_url($form_action_url);
parse_str($parts['query'], $query);
?>
<?php if (isset($query['u']) && isset($query['id'])): ?>
  <div class="opt-in">
      <?php $heading = get_field('newsletter_form_heading', 'option');
      if (!empty($heading)): ?>
        <h2 class="text-2xl leading-normal font-light">
          <?php echo $heading; ?>
        </h2>
      <?php endif; ?>

      <form class="mt-13" action="<?php echo $form_action_url; ?>" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" target="_blank" novalidate>
        <label for="mce-EMAIL" class="sr-only">E-Mail Adresse</label>
        <div id="emailInputGroup" class="flex flex-col md:flex-row rounded-md shadow-solid-xl text-cream p-4 transition duration-300 ease-in-out">
          <div class="flex-grow">
            <input
              id="mce-EMAIL"
              type="email"
              name="EMAIL"
              class="form-input block w-full px-2 py-2.5 text-lg text-dark font-light leading-6 border-none focus:shadow-none transition duration-150 ease-in-out"
              <?php $placeholder = get_field('newsletter_form_placeholder', 'option');
              echo !empty($placeholder) ? 'placeholder="'.$placeholder.'"' : ''; ?>
              onfocus="document.getElementById('emailInputGroup').classList.add('text-light')"
              onblur="document.getElementById('emailInputGroup').classList.remove('text-light')"
              required
            />
          </div>

          <?php $button = get_field('newsletter_form_button', 'option');
          if (!empty($button)): ?>
            <button class="mt-4 md:mt-0 md:ml-4 px-4 py-2.5 text-base leading-6 font-bold rounded text-white bg-primary-500 hover:bg-primary-600 focus:outline-none focus:shadow-outline-primary focus:bg-primary-600 transition duration-150 ease-in-out" name="subscribe" id="mc-embedded-subscribe">
              <?php echo $button; ?>
            </button>
          <?php endif; ?>
        </div>
        <div id="mce-responses">
          <div class="mt-7 -mb-3 text-red-600" id="mce-error-response" style="display:none"></div>
          <div class="mt-7 -mb-3 text-green-600" id="mce-success-response" style="display:none"></div>
        </div>
        <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
        <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_<?php echo $query['u']; ?>_<?php echo $query['id']; ?>" tabindex="-1" value=""></div>
      </form>

      <?php $text = get_field('newsletter_form_text', 'option');
      if (!empty($text)): ?>
        <p class="mt-7 text-sm font-light">
          <?php echo do_shortcode($text); ?>
        </p>
      <?php endif; ?>
    </div>
  </div>
  <script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email'; /*
   * Translated default messages for the $ validation plugin.
   * Locale: DE
   */
  $.extend($.validator.messages, {
    required: "Dieses Feld ist ein Pflichtfeld.",
    maxlength: $.validator.format("Geben Sie bitte maximal {0} Zeichen ein."),
    minlength: $.validator.format("Geben Sie bitte mindestens {0} Zeichen ein."),
    rangelength: $.validator.format("Geben Sie bitte mindestens {0} und maximal {1} Zeichen ein."),
    email: "Geben Sie bitte eine gültige E-Mail Adresse ein.",
    url: "Geben Sie bitte eine gültige URL ein.",
    date: "Bitte geben Sie ein gültiges Datum ein.",
    number: "Geben Sie bitte eine Nummer ein.",
    digits: "Geben Sie bitte nur Ziffern ein.",
    equalTo: "Bitte denselben Wert wiederholen.",
    range: $.validator.format("Geben Sie bitten einen Wert zwischen {0} und {1}."),
    max: $.validator.format("Geben Sie bitte einen Wert kleiner oder gleich {0} ein."),
    min: $.validator.format("Geben Sie bitte einen Wert größer oder gleich {0} ein."),
    creditcard: "Geben Sie bitte ein gültige Kreditkarten-Nummer ein."
  });}(jQuery));var $mcj = jQuery.noConflict(true);</script>
  <!--End mc_embed_signup-->
<?php endif; ?>
