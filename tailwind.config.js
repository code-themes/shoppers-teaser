const defaultTheme = require("tailwindcss/defaultTheme");
const hexRgb = require("hex-rgb");

function rgba(hex, alpha) {
  const { red, green, blue } = hexRgb(hex);
  return `rgba(${red}, ${green}, ${blue}, ${alpha})`;
}

// https://javisperez.github.io/tailwindcolorshades/#/?primary=97C12D&tv=1
const colors = {
  primary: {
    100: "#F5F9EA",
    200: "#E5F0CB",
    300: "#D5E6AB",
    400: "#B6D46C",
    500: "#97C12D",
    600: "#88AE29",
    700: "#5B741B",
    800: "#445714",
    900: "#2D3A0E",
  },
  cream: "#F0F0F0",
  platinum: "#F9FCF9",
  silver: "#E5E5E5",
  light: "#DBDBDB",
  dark: "#27330B",
};

module.exports = {
  theme: {
    extend: {
      borderRadius: {
        xl: "24px",
      },
      boxShadow: {
        "solid-xl": `0 0 0 12px currentColor`,
        "solid-primary": `0 0 0 1px ${colors.primary[500]}`,
        "outline-white": `0 0 0 3px ${rgba(defaultTheme.colors.white, 0.45)}`,
        "outline-primary": `0 0 0 3px ${rgba(colors.primary[500], 0.45)}`,
      },
      colors,
      container: {
        center: true,
        padding: "1.5rem",
      },
      fontFamily: {
        sans: ["Montserrat", ...defaultTheme.fontFamily.sans],
      },
      letterSpacing: {
        "extra-wide": ".35em",
      },
    },
  },
  variants: {
    scale: ["responsive", "hover", "focus", "group-hover", "group-focus"],
    translate: ["responsive", "hover", "focus", "group-hover", "group-focus"],
  },
  plugins: [require("@tailwindcss/ui"), require("tailwindcss-hyphens")],
};
