<?php get_header(); ?>
	
	<div id="content">

		<div id="inner-content" class="wrap">

			<main id="main" class="main" role="main" itemscope itemprop="mainContentOfPage" itemtype="https://schema.org/Blog">

				<article id="post-not-found" class="hentry">

					<div class="container my-20 max-w-4xl">
		        <div class="content" itemprop="articleBody">

							<header class="article-header first-child-mt-0">

								<?php get_template_part( 'templates/header', 'title'); ?>

							</header>

							<section class="entry-content">

								<div class="404-txt last-child-mb-0">

									<p><?php echo __("This isn't the page you're looking for.", 'platetheme'); ?></p>

								</div>

							</section>

						</div>
					</div>

					<footer class="article-footer">

					</footer>

				</article>

			</main>

		</div>

	</div>

    <?php get_sidebar(); ?>

<?php get_footer(); ?>
