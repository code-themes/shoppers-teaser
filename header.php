<!doctype html>

<!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html <?php html_schema(); ?> <?php language_attributes(); ?> class="no-js"><!--<![endif]-->
	<head>

        <?php /**
         * updated with non-blocking order
         * see here: https://csswizardry.com/2018/11/css-and-network-performance/
         * 
         * In short, place any js here that doesn't need to act on css before any css to
         * speed up page loads.
         */
        ?>

        <?php // drop Google Analytics here ?>
        <?php // end analytics ?>

        <?php // See everything you need to know about the <head> here: https://github.com/joshbuchea/HEAD ?>
        <meta charset='<?php bloginfo( 'charset' ); ?>'>
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <?php // favicons (for more: http://www.jonathantneal.com/blog/understand-the-favicon/) ?>
        <link rel="icon" href="<?php echo get_theme_file_uri(); ?>/dist/favicon.png">
        <!--[if IE]>
            <link rel="shortcut icon" href="<?php echo get_theme_file_uri(); ?>/dist/favicon.ico">
        <![endif]-->

        <!-- Apple Touch Icon -->
        <link rel="apple-touch-icon" href="<?php echo get_theme_file_uri(); ?>/dist/icon.png">

        <?php // updated pingback. Thanks @HardeepAsrani https://github.com/HardeepAsrani ?>
        <?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
            <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
        <?php endif; ?>

        <?php // put font scripts like Typekit/Adobe Fonts here ?>
        <?php // end fonts ?>

        <?php // wordpress head functions ?>
        <?php wp_head(); ?>
        <?php // end of wordpress head ?>

    </head>
	<body <?php body_class(); ?> itemscope itemtype="https://schema.org/WebPage">

		<div id="container">

            <?php if (!is_page_template('page-home.php')): ?>
                <header class="container my-14 max-w-4xl" role="banner" itemscope itemtype="https://schema.org/WPHeader">
                  <div class="flex justify-between" itemscope itemtype="https://schema.org/Organization">
                    <div itemprop="logo">
                        <a href="<?php echo home_url(); ?>" class="block focus:outline-none focus:shadow-outline-primary rounded transition duration-150 ease-in-out" rel="nofollow" itemprop="url">
                            <?php echo load_svg('src/images/logo.svg', 'h-8 w-auto'); ?>
                        </a>
                    </div>

                    <nav class="-mr-4" role="navigation" itemscope itemtype="https://schema.org/SiteNavigationElement" aria-label="<?php _e('Primary Menu ', 'platetheme'); ?>">
                        <?php wp_nav_menu(array(
                            'container' => false, // remove nav container
                            'menu' => __( 'Main Menu', 'platetheme' ), // nav name
                            'menu_class' => 'main-menu', // adding custom nav class
                            'theme_location' => 'main-nav', // where it's located in the theme
                            'depth' => 0, // limit the depth of the nav
                        )); ?>
                    </nav>
                  </div>
                </header>
            <?php endif; ?>
