module.exports = {
  plugins: [
    require("tailwindcss"),
    require("autoprefixer"),
    require("postcss-nested"),
    require("postcss-font-magician")({
      foundries: "google",
      variants: {
        Montserrat: {
          "100": [],
          "200": [],
          "300": [],
          "400": [],
          "500": [],
          "600": [],
          "700": [],
          "800": [],
          "900": [],
          "100 italic": [],
          "200 italic": [],
          "300 italic": [],
          "400 italic": [],
          "500 italic": [],
          "600 italic": [],
          "700 italic": [],
          "800 italic": [],
          "900 italic": [],
        },
      },
    }),
  ],
};
